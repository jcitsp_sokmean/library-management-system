package model;

import java.io.Serializable;

public class BookCopy implements Serializable {
	
	private Book book;
	private String copyNo;
	private boolean availability;

	BookCopy(Book book, String copyNo, boolean avialable){
		this.book = book;
		this.copyNo = copyNo;
		this.availability = avialable;
	}

	public BookCopy() {
	}

	public String getCopyId() {
		return copyNo; 
	}
	public void setCopyId(String copyId) {
		this.copyNo = copyId;
	}
	public boolean isAvailability() {
		return availability;
	}
	public void setAvailability(boolean availability) {
		this.availability = availability;
	}
	public Book getBook() {
		return book;
	}
	
	public void setBook(Book book) {
		this.book = book;
	}
	
}
