package model;

import java.io.Serializable;

import model.CheckOutRecord;

public class LibraryMember extends Person implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String memberId;
	private CheckOutRecord checkoutRecord;
	public LibraryMember(){
		
	}
	
	public LibraryMember(String firstName, 
			String lastName,
			String phoneNo,
			Address address,
			String memberId) 
	{
		
		super(firstName, lastName, phoneNo, address);
		this.memberId = memberId;
		this.checkoutRecord=null;
	}
	
	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	
	@Override
	public String toString()
	{
		String output = "";
		output += getFirstName() + ":"+getLastName() + ":" +getMemberId();
		return output;
	}

}
