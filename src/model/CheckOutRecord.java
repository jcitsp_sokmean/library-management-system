package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CheckOutRecord implements Serializable {
	
	private Date paidDate;
	private double fine;
	
	private LibraryMember libraryMember = new LibraryMember();
    private List<CheckOutRecordEntry> checkoutRecordEntries=new ArrayList<CheckOutRecordEntry>();
	
	public CheckOutRecord(Date paidDate, double fine, LibraryMember libraryMember) {
		this.paidDate = paidDate;
		this.fine = fine;
		this.libraryMember = libraryMember;
	}
	
	public Date getPaidDate() {
		return paidDate;
	}
	public void setPaidDate(Date paidDate) {
		this.paidDate = paidDate;
	}
	public double getFine() {
		return fine;
	}
	public void setFine(double fine) {
		this.fine = fine;
	}
	public LibraryMember getLibraryMember() {
		return libraryMember;
	}
	public void setLibraryMember(LibraryMember libraryMember) {
		this.libraryMember = libraryMember;
	}

	public List<CheckOutRecordEntry> getCheckoutRecordEntries() {
		return checkoutRecordEntries;
	}

	public void setCheckoutRecordEntries(List<CheckOutRecordEntry> checkoutRecordEntries) {
		this.checkoutRecordEntries = checkoutRecordEntries;
	}
	
	public void addEntry(CheckOutRecordEntry checkoutRecordEntry){		
		checkoutRecordEntries.add(checkoutRecordEntry);
	}
}
