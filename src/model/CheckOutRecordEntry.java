package model;

import java.io.Serializable;
import java.time.LocalDate;

public class CheckOutRecordEntry implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private LocalDate checkOutDate;
	private LocalDate dueDate;
	private BookCopy bookCopy;
	private double fine;
	
	public CheckOutRecordEntry(LocalDate checkOutDate, LocalDate dueDate, BookCopy bookCopy, double fine) {
		this.checkOutDate = checkOutDate;
		this.dueDate = dueDate;
		this.bookCopy = bookCopy;
		this.fine = fine;
	}
	

	public LocalDate getCheckoutDate() {
		return checkOutDate;
	}

	public void setCheckoutDate(LocalDate checkoutDate) {
		this.checkOutDate = checkoutDate;
	}

	public LocalDate getDueDate() {
		return dueDate;
	}

	public void setDueDate(LocalDate dueDate) {
		this.dueDate = dueDate;
	}


	public double getFine() {
		return fine;
	}

	public void setFine(double fine) {
		this.fine = fine;
	}

	public BookCopy getCopy() {
		return bookCopy;
	}

	public void setCopy(BookCopy copy) {
		this.bookCopy = copy;
	}

}
