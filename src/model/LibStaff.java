package model;

import java.io.Serializable;

public class LibStaff extends Person implements Serializable {
	
	private String userName;
	private String password;
	private String staffId;

	public LibStaff() {

	}

	public LibStaff(String name, String pwd) {
		this.userName = name;
		this.password = pwd;
	}

	public LibStaff(String name, String pwd, String staffId) {
		this.userName = name;
		this.password = pwd;
		this.staffId = staffId;
	}

	public void setUserName(String user) {
		this.userName = user;
	}

	public String getUserName() {
		return userName;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPassword() {
		return password;
	}

	public void setStaffId(String id) {
		this.staffId = id;
	}

	public String getStaffId() {
		return staffId;
	}

	@Override
	public String toString() {
		return userName + " : " + password + " : " + staffId;
	}

}