package model;

import java.util.List;
import java.io.Serializable;
import java.util.ArrayList;

public class Book implements Serializable{
	
	private String ISBN;
	private String title;
	private String maxCheckLength;
	private String numCopies;
	
	private List<Author> authors = new ArrayList<Author>();
	private List<BookCopy> books = new ArrayList<BookCopy>();
	
	public Book(String isbn, String title, String maxLength, List<Author> author,String numCopies ){
		this.ISBN = isbn;
		this.title = title;
		this.maxCheckLength = maxLength;
		this.authors = author;
		
		this.numCopies = numCopies;
		for (int i = 0; i < Integer.parseInt(numCopies); i++) {
			addBookCopy(i);
		}
	}
	
	public Book(String isbn, String title, String maxLength,String numCopies1 ){
		this.ISBN = isbn;
		this.title = title;
		this.maxCheckLength = maxLength;
		this.numCopies = numCopies1;
		/*for (int i = 0; i < Integer.parseInt(numCopies); i++) {
			addBookCopy(i);
		}*/
	}
	
	
    public Book() {
    }
    
	public String getISBN() {
		return ISBN;
	}

	public void setISBN(String iSBN) {
		ISBN = iSBN;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMaxCheckLength() {
		return maxCheckLength;
	}

	public void setMaxCheckLength(String maxCheckLength) {
		this.maxCheckLength = maxCheckLength;
	}
	
	public int getNumCopies(){
		return Integer.parseInt(numCopies);
	}

	public List<BookCopy> getBooks() {
		return books;
	}

	public void setBooks(List<BookCopy> books) {
		this.books = books;
	}

	public List<Author> getAuthors() {
		return authors;
	}

	public void setAuthors(List<Author> authors) {
		this.authors = authors;
	}
	
	public void addAuthor(Author author){
		this.authors.add(author);
	}
	
	public List<BookCopy> getBookCopy(){
		return books;
	}
	
	public void addBookCopy(int numCopy) {
		int lastSize = books.size() + 1;
		BookCopy copy = new BookCopy(this,this.ISBN +"-"+lastSize, true);
		books.add(copy);
	}
}
