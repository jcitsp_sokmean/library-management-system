package model;

import java.io.Serializable;

public class Person implements Serializable{
	
    private String firstName;

    private String lastName;
    private String phone;
    private String phoneNumber;

    private Address address;
    
	public Person() {
		
    }

    public Person(String firstName, String lastName, String phoneNumber,Address address) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.phoneNumber = phoneNumber;
		this.address = address;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
	
	public Address getAddress(){
		return address;
	}

}