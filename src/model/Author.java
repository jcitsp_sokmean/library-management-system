package model;

import java.io.Serializable;

public class Author extends Person implements Serializable {
	
    private String credential;

    private String shorBio;
    
    private String zip;
    
	public Author(String firstName, String lastName, String phoneNo, Address address, String credential, String shorBio) {
		super(firstName, lastName, phoneNo, address);
		this.credential = credential;
		this.shorBio = shorBio;
	}
	
	public String getCredential() {
		return credential;
	}

	public void setCredential(String credential) {
		this.credential = credential;
	}

	public String getShorBio() {
		return shorBio;
	}

	public void setShorBio(String shorBio) {
		this.shorBio = shorBio;
	}

	public Author(String credential, String shorBio) {
		this.credential = credential;
		this.shorBio = shorBio;
	}

	@Override
	public String toString() {
		return this.getFirstName()+this.getLastName();
	}

	public String getAddr(){
		if(super.getAddress()!= null){
			return super.getAddress().toString();
		}
		return "";
	}
	
	
	
	
	
}
