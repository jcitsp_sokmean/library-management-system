package controller;


import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import dataaccess.BookCopyObj;
import dataaccess.BookObj;
import dataaccess.LibMemberObj;
import dataaccess.LibStaffObj;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import model.Book;
import model.BookCopy;
import model.LibraryMember;
import util.Helper;

public class StaffBoard implements Initializable{

	
	static Helper helper = new Helper();
	Stage stage;
	
	@FXML
	private Label lblLogger;
	@FXML 
	private TextField txtUserId;
	@FXML
	private TextField txtISBN;
	
	@FXML
	private Pane staffPane;
	
	@FXML
	private Pane adminPane;
	
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		lblLogger.setText("Logged as :"+LoginController.logger.getUserName());
		if(LoginController.logger.getStaffId().equals("1"))
		{
			staffPane.setDisable(true);
			adminPane.setDisable(false);
		}else{
			staffPane.setDisable(false);
			adminPane.setDisable(true);
		}
	}
	@FXML
	public void listBookAction() {
		
		String memberView = "../view/layout/Book.fxml";
		String viewTitle = "Library System";

		helper.loadNewStage(stage, lblLogger, memberView, viewTitle, false);
		
	}
	@FXML
	public void addBookAction() {
		String memberView = "../view/layout/AddBook.fxml";
		String viewTitle = "Library System";

		helper.loadNewStage(stage, lblLogger, memberView, viewTitle, false);
	}
	@FXML
	public void listMemberAction() {
		String memberView = "../view/layout/Member.fxml";
		String viewTitle = "Library System";

		helper.loadNewStage(stage, lblLogger, memberView, viewTitle, false);
	}
	@FXML
	public void addMemberAction() {
		String memberView = "../view/layout/AddNewMember.fxml";
		String viewTitle = "Library System";

		helper.loadNewStage(stage, lblLogger, memberView, viewTitle, false);
	}
	
	@FXML
	public void listAuthor() {
		String memberView = "../view/layout/Author.fxml";
		String viewTitle = "Library System";

		helper.loadNewStage(stage, lblLogger, memberView, viewTitle, false);
	}
	
	@FXML
	public void checkoutRecordAction() {

	}
	
	@FXML
	public void checkoutAction() {

		LibraryMember member = new LibraryMember();
		Book book = new Book();
		
		List<BookCopy> listBookCpy = new ArrayList<BookCopy>();
		
		LibMemberObj serviceMem = new LibMemberObj();
		BookObj serviceBook = new BookObj();
		BookCopyObj serviceCpy = new BookCopyObj();
		
		member = serviceMem.searchMemberId(txtUserId.getText());
		
		
		
		if (member != null) {
			book = serviceBook.searchBook(txtISBN.getText());
			if (book != null) {
				
//				for(int i = 0 ; i < listBookCpy.size(); i++){
//					if(listBookCpy.get(i).getCopyId().equals(txtISBN.getText()))
//					{
//						if(listBookCpy.get(i).isAvailability()){
//							listBookCpy.get(i).setAvailability(false);
//							break;
//						}
//					}
//				}
//				serviceCpy.editeBookCopy(listBookCpy);
				helper.showSuccessDialog("Book has been checked out", "Message");
			} else {
				helper.showErroDialog("The book is not avialable to checkout", "Message");
			}
		} else {
			helper.showErroDialog("Invalid user", "Massage");
		}
		
	}
	
	@FXML
	public void logOutAction() {

		LoginController.logger = null;
		
		String memberView = "../view/layout/Login.fxml";
		String viewTitle = "Library System";

		helper.loadNewStage(stage, lblLogger, memberView, viewTitle, false);
	}
}
