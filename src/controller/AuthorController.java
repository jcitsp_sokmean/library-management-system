package controller;
import java.net.URL;
import java.util.ResourceBundle;

import util.Helper;
import dataaccess.AuthorObj;
import model.Address;
import model.Author;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
public class AuthorController implements Initializable{

	Stage stage;
	Helper helper=new Helper();


	@FXML
	private TextField txtAuthorFName;
	@FXML
	private TextField txtAuthorLName;
	@FXML
	private TextField txtAuthorPhone;
	@FXML
	private TextField txtAuthorStreet;
	@FXML
	private TextField txtAuthorCity;
	@FXML
	private TextField txtAuthorState;
	@FXML
	private TextField txtAuthorZip;
	@FXML
	private TextField txtAuthorShortBio;
	@FXML
	private TextField txtAuthorCredential;
	
	@FXML 
	private Label lblTitle;
	
	@FXML
	public void backAction()
	{
		String memberView = "../view/layout/StaffBoard.fxml";
		String viewTitle = "Library System";
		LoginController.helper.loadNewStage( stage, lblTitle, memberView, viewTitle, false);
	}
	

	@FXML
	public void addNewAuthor(){
		Address address = new Address(txtAuthorStreet.getText(), txtAuthorCity.getText(), txtAuthorState.getText(),
				txtAuthorZip.getText());
		Author newAuthor = new Author(txtAuthorFName.getText(), txtAuthorLName.getText(), txtAuthorPhone.getText(),
				address, txtAuthorCredential.getText(), txtAuthorShortBio.getText());
		AuthorObj authorImpl = new AuthorObj();
		authorImpl.addAuthor(newAuthor);
		LoginController.helper.showSuccessDialog("New auhtor was added to the system", "Save successfully");
		
	};

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	};
}
