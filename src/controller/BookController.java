package controller;

import model.Book;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import dataaccess.BookObj;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class BookController implements Initializable {
	
	BookObj book=new BookObj();
	List<Book> bookList=book.getAllItems();
	
	
	@FXML
	private TableView<Book>  bookTable;
	
	@FXML 
	private TableColumn<Book, String> colISBN;
	
	@FXML 
	private TableColumn<Book, String> colTitle;
	
	@FXML 
	private TableColumn<Book, String> colAuthor;
	
	@FXML 
	private TableColumn<Book, String> colCopy;
	
	@FXML
	private Button check;
	
	@FXML
	private Label lblTitle;
	
	Stage stage;
	
	@FXML
	public void backAction()
	{
		String memberView = "../view/layout/StaffBoard.fxml";
		String viewTitle = "Library System";
		LoginController.helper.loadNewStage( stage, lblTitle, memberView, viewTitle, false);
	}
	
	
	public void getAllBooks() {
		colISBN.setCellValueFactory(new PropertyValueFactory<Book, String>("ISBN"));
		colTitle.setCellValueFactory(new PropertyValueFactory<Book, String>("title"));
		colAuthor.setCellValueFactory(new PropertyValueFactory<Book,String>("maxCheckLength"));
		colCopy.setCellValueFactory(new PropertyValueFactory<Book, String>("numCopies"));
		bookTable.setItems(FXCollections.observableArrayList(bookList));
	}


@Override
public void initialize(URL location, ResourceBundle resources) {
	// TODO Auto-generated method stub
	//getAllBooks();
	
	Platform.runLater(() -> {
		getAllBooks();
	});
}
	
	
	
	
}
