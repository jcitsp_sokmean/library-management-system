package controller;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import dataaccess.LibMemberObj;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import model.Book;
import model.LibraryMember;
import util.Helper;

public class MemberController implements Initializable{
	
	LibMemberObj libMember=new LibMemberObj();
	List<LibraryMember> memberList=libMember.getAllItems();
	
	@FXML
	private TableView<LibraryMember>  memberTable;
	
	@FXML 
	private TableColumn<LibraryMember, String> colMember;
	
	@FXML 
	private TableColumn<LibraryMember, String> colFirst;
	
	@FXML 
	private TableColumn<LibraryMember, String> colLast;
	
	@FXML 
	private TableColumn<LibraryMember, String> colPhone;
	
	@FXML 
	private TableColumn<LibraryMember, String> colStreet;
	
	@FXML 
	private TableColumn<LibraryMember, String> colCity;
	
	@FXML 
	private TableColumn<LibraryMember, String> colState;
	
	@FXML 
	private TableColumn<LibraryMember, String> colZip;
	
	@FXML 
	private Label lblTitle;
	
	Stage stage;
	public void getAllMembers() {
	
		colMember.setCellValueFactory(new PropertyValueFactory<LibraryMember, String>("memberId"));
		colFirst.setCellValueFactory(new PropertyValueFactory<LibraryMember, String>("firstName"));
		colLast.setCellValueFactory(new PropertyValueFactory<LibraryMember,String>("lastName"));
		colPhone.setCellValueFactory(new PropertyValueFactory<LibraryMember, String>("phone"));
		colStreet.setCellValueFactory(cellData -> cellData.getValue().getAddress().getStreetProperty());
		colCity.setCellValueFactory(cellData -> cellData.getValue().getAddress().getCityProperty());
		colState.setCellValueFactory(cellData -> cellData.getValue().getAddress().getStateProperty());
		colZip.setCellValueFactory(cellData -> cellData.getValue().getAddress().getZipProperty());
		memberTable.setItems(FXCollections.observableArrayList(memberList));
	}
	
	@FXML
	public void backAction()
	{
		String memberView = "../view/layout/StaffBoard.fxml";
		String viewTitle = "Library System";
		LoginController.helper.loadNewStage( stage, lblTitle, memberView, viewTitle, false);
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		Platform.runLater(() -> {
			getAllMembers();
		});
	}

}
