package controller;

import java.net.URL;
import java.util.ResourceBundle;

import dataaccess.LibStaffObj;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.LibStaff;
import model.LibStaffRoles;
import util.Helper;

public class LoginController implements Initializable {

	static LibStaff logger;
	
	LibStaffRoles role;
	
	@FXML
	private Label lblUserName;
	@FXML
	private TextField usernameField;
	@FXML
	private PasswordField passwordField;
	static Helper helper = new Helper();
	Stage stage;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
	}
	
	@FXML
	public void loginAction() {
	
		LibStaffObj staff = new LibStaffObj();
		logger = staff.searchStaffId(usernameField.getText());
	
		if (logger != null) {
			if (logger.getPassword().toString().equals(passwordField.getText())) {
				String memberView = "../view/layout/StaffBoard.fxml";
				String viewTitle = "Library System";

				helper.loadNewStage(stage, lblUserName, memberView, viewTitle, false);

			} else {
				helper.showErroDialog("Invalid userID or password", "Login Error");
			}
		} else {
			helper.showErroDialog("Invalid userID or password", "Login Error");
		}
	}
	
}
