package controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import javafx.scene.control.*;
import model.Address;
import model.LibraryMember;

import java.net.URL;
import java.util.ResourceBundle;

import util.Helper;
import controller.LoginController;
import dataaccess.LibMemberObj;
import dataaccess.LibMemberObj;

public class AddNewMember implements Initializable {
	
	@FXML
	private TextField txtMemberID;
	@FXML
	private TextField txtMemberFName;
	@FXML
	private TextField txtMemberLName;
	@FXML
	private TextField txtMemberPhone;
	@FXML
	private TextField txtMemberStreet;
	@FXML
	private TextField txtMemberCity;
	@FXML
	private TextField txtMemberState;
	@FXML
	private TextField txtMemberZip;
	@FXML
	private Label lblTitle;
	@FXML
	private Button btnBack;
	@FXML
	private Button btnMemberInfoClear;
	@FXML
	private Button btnMemberInfoSave;
	
	Stage stage;
	
	private Address memAddr;
	
	@FXML
	public void backAction(){
		String memberView = "../view/layout/StaffBoard.fxml";
		String viewTitle = "Library System";
		LoginController.helper.loadNewStage( stage, lblTitle, memberView, viewTitle, false);
	}
	
	public void addNewMember() {
		
		Address address = new Address(txtMemberStreet.getText(), txtMemberCity.getText(), txtMemberState.getText(),
				txtMemberZip.getText());
		LibraryMember newLibraryMember = new LibraryMember();

		newLibraryMember.setMemberId(txtMemberID.getText());
		newLibraryMember.setFirstName(txtMemberFName.getText());
		newLibraryMember.setLastName(txtMemberLName.getText());
		newLibraryMember.setPhone(txtMemberPhone.getText());
		newLibraryMember.setAddress(address);

		LibMemberObj memberImpl = new LibMemberObj();
		memberImpl.addLibraryMember(newLibraryMember);
		LoginController.helper.showSuccessDialog("New Member was added to the system", "Save successfully");
		
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
	}
}
