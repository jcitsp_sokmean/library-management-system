package controller;

import javafx.scene.*;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import controller.LoginController;
import dataaccess.BookCopyObj;
import dataaccess.BookObj;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.*;

public class AddNewBookController implements Initializable{

	
	@FXML
	private Label lblTitle;
	
	@FXML
	private TextField txtISBN;
	@FXML
	private TextField txtTitle;
	@FXML
	private TextField txtMaxLenght;
	@FXML
	private TextField txtNoCopy;
	@FXML
	
	private TextField txtFirstName;
	@FXML
	private TextField txtLastName;
	@FXML
	private TextField txtPhone;
	@FXML
	private TextArea txtAdress;
	@FXML
	private TextField txtCredentail;
	@FXML
	private TextArea txtAbout;
	@FXML
	
	private List<Author> authors = new ArrayList<Author>();
	Stage stage;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
	}
	
	@FXML
	public void backAction(){
		//LoginController.helper.backToHome(stage, lblTitle, "../view/StaffBoard.fxml");
		String memberView = "../view/layout/StaffBoard.fxml";
		String viewTitle = "Library System";

		LoginController.helper.loadNewStage(stage, lblTitle, memberView, viewTitle, false);
	}
	
	@FXML
	public void saveAction(){
		//Book newBook1 = new Book("ISBN","Title1","50","5");
		Book newBook = new Book(txtISBN.getText(), txtTitle.getText(), txtMaxLenght.getText(),authors, txtNoCopy.getText());
		
		BookObj bookImp1 = new BookObj();
		BookCopyObj bookCpy = new BookCopyObj();
		
		bookImp1.addBook(newBook);
		bookCpy.addBookCopy(newBook.getBookCopy());
		
		LoginController.helper.showSuccessDialog("New book was successfully saved", "Save successfully");
	}
	
	@FXML
	public void addAuthorAction(){
		Author author = new Author(
				txtFirstName.getText(),
				txtLastName.getText(),
				txtPhone.getText(),
				new Address(txtAdress.getText(),"","",""), 
				txtCredentail.getText(),
				txtAbout.getText());
		authors.add(author);
	}
}
