package start;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.LibStaff;
import util.Helper;

public class Main extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		
		Helper helper = new Helper();
	
		String loginView = "../view/layout/Login.fxml";
		String viewTitle = "User Login";
		helper.loadView(stage, loginView, viewTitle,false);
		
//		String loginView = "../view/layout/StaffBoard.fxml";
//		String viewTitle = "User Login";
//		helper.loadView(stage, loginView, viewTitle,false);

		
	}
}

