package dataaccess;

import java.util.ArrayList;
import java.util.List;

import model.BookCopy;

public interface BookCopyInt {
	public void addBookCopy(List<BookCopy> bookCpy);
	public BookCopy searchBookCopy(String ISBN);
	public List<BookCopy> getBookCpy(String ISBN);
}
