package dataaccess;

import model.Author;

public interface AuthorIn {
	public void addAuthor(Author newAuthor);
}
