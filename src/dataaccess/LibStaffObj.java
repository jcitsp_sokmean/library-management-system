package dataaccess;

import java.util.ArrayList;
import java.util.List;

import model.LibStaff;

public class LibStaffObj extends DataAccess {
	public LibStaffObj() {
		firstTime();
	}

	public LibStaff editLibStaff(LibStaff newStaff, String username, String password) {
		editObject(newStaff, username, password);
		return null;
	}

	public boolean addLibStaff(LibStaff newStaff) {
		// List<LibStaff> libstaff = saveObject(newStaff);
		return true;
	}

	public LibStaff searchStaffId(String id) {

		List<LibStaff> allUser = getAllItems();
		for (LibStaff staff : allUser) {
			
			if (staff.getStaffId().toString().equals(id)) {
				return staff;
			}
		}

		return null;
	}

	public void firstTime() {
//		LibStaff admin = new LibStaff("admin", "@123","1");
//		LibStaff librarian = new LibStaff("librarian", "@123","2");
//		List<LibStaff> myList = new ArrayList<LibStaff>();
//		myList.add(admin);
//		myList.add(librarian);
//		save(myList);
//		System.out.println("Successfully saved an object");
	}
}
