package dataaccess;

import java.util.List;

import model.LibStaff;
import model.LibraryMember;

public class LibMemberObj extends DataAccess {
	public void editLibMemberObj(LibraryMember newMember, String username, String password) {
		editMemberObject(newMember, username, password);
	}

	public boolean addLibMember(LibraryMember newMember) {
		saveObject(newMember);
		return true;
	}

	public void addLibraryMember(LibraryMember newLibraryMember){
		List<LibraryMember> allLibraryMember = getAllItems();
		allLibraryMember.add(newLibraryMember);
		save(allLibraryMember);
	}
	public LibraryMember searchMemberId(String id) {

		List<LibraryMember> allUser = getAllItems();
		for (LibraryMember member : allUser) {
			if (member.getMemberId().equals(id)) {
				return member;
			}
		}
		return null;
	}
}
