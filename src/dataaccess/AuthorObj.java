package dataaccess;

import java.util.List;
import model.Author;
import model.Book;

public class AuthorObj extends DataAccess implements AuthorIn{

	@Override
	public void addAuthor(Author newAuthor) {
		List<Author> allAuthor = getAllItems();
		allAuthor.add(newAuthor);
		save(allAuthor);
		
	}
	
/*	@Override
	public Book searchBook(String requestId) {
		List<Book> allBook = getAllItems();
		for (Book book : allBook) {
			if (book.getISBN().toString().equals(requestId)) {
				return book;
			}
		}

		return null;

	}

	@Override
	public void addBook(Book book) {
		List<Book> allBook = getAllItems();
		allBook.add(book);
		save(allBook);
	}*/
	
}
