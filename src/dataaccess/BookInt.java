package dataaccess;

import model.Book;

public interface BookInt {
	public void addBook(Book currentUser);
	public Book searchBook(String requestId);
}
