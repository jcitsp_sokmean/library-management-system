package dataaccess;

import java.util.ArrayList;
import java.util.List;

import model.Book;
import model.BookCopy;

public class BookCopyObj extends DataAccess implements BookCopyInt {

	@Override
	public void addBookCopy(List<BookCopy> list) {
		List<BookCopy> allBook = getAllItems();
		for(BookCopy bCpy : list)
		allBook.add(bCpy);
		save(allBook);
	}

	@Override
	public BookCopy searchBookCopy(String ISBN) {
		List<BookCopy> allBook = getAllItems();
		for (BookCopy bookCpy : allBook) {
			if (bookCpy.getCopyId().toString().equals(ISBN)) {
				return bookCpy;
			}
		}

		return null;
	}
	

	public void editeBookCopy(List<BookCopy> bookCpy) {
		List<BookCopy> list = getAllItems();
		
		if(list != null && bookCpy != null)
		{
			for (int i = 0; i < list.size(); i++) {
				if(list.get(i).getCopyId().equals(bookCpy.get(0).getBook().getISBN())){
					list.remove(i);
				}
			}
			
			for(BookCopy b : bookCpy){
				list.add(b);
			}
			save(list);
		}
	}

	@Override
	public List<BookCopy> getBookCpy(String ISBN) {
		
		List<BookCopy> list = new ArrayList<BookCopy>();
		List<BookCopy> allBook = getAllItems();
		
		for (BookCopy bookCpy : allBook) {
			if (bookCpy.getBook().getISBN().toString().equals(ISBN)) {
				list.add(bookCpy);
			}
		}
		
		return list;
	}
	
	
	public void updateBookCopy(List<BookCopy> list) {
		save(list);
	}
}
